package com.jlngls.bancojln.Activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentTransaction;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;

import com.jlngls.bancojln.Fragment.ContatosFragment;
import com.jlngls.bancojln.Fragment.ConversasFragment;
import com.jlngls.bancojln.Fragment.StatusFragment;
import com.jlngls.bancojln.R;

public class MainActivity extends AppCompatActivity {


    private Button buttonConversas, buttonContatos, buttonStatus;
    private ConversasFragment conversasFragment;
    private ContatosFragment contatosFragment;
    private StatusFragment statusFragment;
    private FrameLayout frameConteudo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        buttonContatos = findViewById(R.id.buttonContato);
        buttonConversas = findViewById(R.id.buttonConversa);
        buttonStatus = findViewById(R.id.buttonStatus);

        // remover sombra do action bar
        getSupportActionBar().setElevation(0);


        buttonContatos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                contatosFragment = new ContatosFragment();
                FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
                transaction.replace(R.id.frameConteudo,contatosFragment);
                transaction.commit();
            }
        });

        buttonConversas.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                conversasFragment = new ConversasFragment();
                FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
                transaction.replace(R.id.frameConteudo,conversasFragment);
                transaction.commit();
            }
        });

        buttonStatus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                statusFragment = new StatusFragment();

                FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
                transaction.replace(R.id.frameConteudo,statusFragment);
                transaction.commit();

            }
        });
    }
}
