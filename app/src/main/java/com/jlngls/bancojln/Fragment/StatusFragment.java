package com.jlngls.bancojln.Fragment;

import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.jlngls.bancojln.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class StatusFragment extends Fragment {
    private TextView textStatus;

    public StatusFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_status, container, false);

        textStatus = view.findViewById(R.id.textStatus);
        textStatus.setText("Status");
        return  view;
    }
}
